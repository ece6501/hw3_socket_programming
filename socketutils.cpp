#include "socketutils.hpp"

using namespace std;

int makeServer(const char* port) {
  int status;
  int socket_fd;
  struct addrinfo host_info;
  struct addrinfo* host_info_list;

  memset(&host_info, 0, sizeof(host_info));
  host_info.ai_family = AF_UNSPEC;
  host_info.ai_socktype = SOCK_STREAM;  // TCP
  host_info.ai_flags = AI_PASSIVE;      // use my IP
  // host_info.ai_flags = INADDR_ANY;

  status = getaddrinfo(NULL, port, &host_info, &host_info_list);
  if (status != 0) {
    cerr << "Error: cannot get address info for host" << endl;
    exit(EXIT_FAILURE);
  }

  // Ask os to automatically assign idle port
  if (strlen(port) == 0) {
    struct sockaddr_in* addr_in =
        (struct sockaddr_in*)(host_info_list->ai_addr);
    addr_in->sin_port = 0;
  }

  socket_fd = socket(host_info_list->ai_family, host_info_list->ai_socktype,
                     host_info_list->ai_protocol);
  if (socket_fd == -1) {
    cerr << "Error: cannot create socket" << endl;
    exit(EXIT_FAILURE);
  }

  int yes = 1;
  status = setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
  status = bind(socket_fd, host_info_list->ai_addr, host_info_list->ai_addrlen);
  if (status == -1) {
    cerr << "Error: cannot bind socket" << endl;
    exit(EXIT_FAILURE);
  }

  status = listen(socket_fd, 100);
  if (status == -1) {
    cerr << "Error: cannot listen on socket" << endl;
    exit(EXIT_FAILURE);
  }

  freeaddrinfo(host_info_list);
  return socket_fd;
}

/**
 * Accept client and restore the client's to ip.
 **/

int acceptClient(int socket_fd, string& ip) {
  struct sockaddr_storage socket_addr;
  socklen_t socket_addr_len = sizeof(socket_addr);
  int client_connection_fd;

  client_connection_fd =
      accept(socket_fd, (struct sockaddr*)&socket_addr, &socket_addr_len);
  if (client_connection_fd == -1) {
    cerr << "Error: cannot accept connection on socket" << endl;
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in* addr = (struct sockaddr_in*)&socket_addr;

  ip = inet_ntoa(addr->sin_addr);

  return client_connection_fd;
}

int connectToServer(const char* hostName, const char* port) {
  int status;
  int socket_fd;
  struct addrinfo host_info;
  struct addrinfo* host_info_list;

  memset(&host_info, 0, sizeof(host_info));
  host_info.ai_family = AF_UNSPEC;
  host_info.ai_socktype = SOCK_STREAM;  // TCP

  status = getaddrinfo(hostName, port, &host_info, &host_info_list);
  if (status != 0) {
    cerr << "Error: cannot get address info for host" << endl;
    cerr << "  (" << hostName << "," << port << ")" << endl;
    exit(EXIT_FAILURE);
  }

  socket_fd = socket(host_info_list->ai_family, host_info_list->ai_socktype,
                     host_info_list->ai_protocol);
  if (socket_fd == -1) {
    cerr << "Error: cannot create socket" << endl;
    cerr << "  (" << hostName << "," << port << ")" << endl;
    exit(EXIT_FAILURE);
  }

  status =
      connect(socket_fd, host_info_list->ai_addr, host_info_list->ai_addrlen);
  if (status == -1) {
    cerr << "Error: cannot connect to socket" << endl;
    cerr << "  (" << hostName << "," << port << ")" << endl;
    exit(EXIT_FAILURE);
  }

  freeaddrinfo(host_info_list);
  return socket_fd;
}

int getPortNumber(int socket_fd) {
  int status;
  struct sockaddr_in addr;
  socklen_t len = sizeof(addr);
  status = getsockname(socket_fd, (struct sockaddr*)&addr, &len);
  if (status == -1) {
    cerr << "getsockname failed." << endl;
    exit(EXIT_FAILURE);
  }
  return ntohs(
      addr.sin_port);  // make byte order compatible with host architecture
}