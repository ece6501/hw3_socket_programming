all: ringmaster player

ringmaster: ringmaster.cpp socketutils.cpp potato.hpp
	g++ -o $@ ringmaster.cpp socketutils.cpp

player: player.cpp socketutils.cpp potato.hpp
	g++ -o $@ player.cpp socketutils.cpp

.PHONY:
	clean

clean:
	rm -rf *.o *~ ringmaster player