#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <algorithm>
#include <vector>

#include "potato.hpp"
#include "socketutils.hpp"

using namespace std;

int main(int argc, char* argv[]) {
  if (argc != 3) {
    cerr << "player <machine_name> <port_num>" << endl;
    return EXIT_FAILURE;
  }

  const char* hostname = argv[1];
  const char* port = argv[2];
  int playerId, numPlayers;

  int master = connectToServer(hostname, port);
  recv(master, &playerId, sizeof(playerId), MSG_WAITALL);
  recv(master, &numPlayers, sizeof(numPlayers), MSG_WAITALL);

  int currPlayer = makeServer("");
  int playerPort = getPortNumber(currPlayer);
  send(master, &playerPort, sizeof(playerPort), 0);
  cout << "Connected as player " << playerId << " out of " << numPlayers
       << " total palyers" << endl;

  int rightPort, ipCharLen;
  recv(master, &rightPort, sizeof(rightPort), MSG_WAITALL);
  recv(master, &ipCharLen, sizeof(ipCharLen), MSG_WAITALL);
  char rightIp[ipCharLen];
  recv(master, rightIp, sizeof(rightIp), MSG_WAITALL);

  // Connect to its right and accpets its left to communicate
  string rightPortStr = to_string(rightPort);
  int right = connectToServer(rightIp, rightPortStr.c_str());
  int rightId = (playerId + 1) % numPlayers;
  string leftIp;
  int left = acceptClient(currPlayer, leftIp);
  int leftId = (playerId + numPlayers - 1) % numPlayers;
  
  // Combine information
  vector<int> connected{left, right, master};
  vector<int> neighborIds{leftId, rightId};

  fd_set toMonitor;
  int maxSocketNum = *(max_element(connected.begin(), connected.end()));
  Potato potato;
  srand((unsigned int)time(NULL) + playerId);

  while (true) {
    FD_ZERO(&toMonitor);
    for (int i = 0; i < connected.size(); ++i) {
      FD_SET(connected[i], &toMonitor);
    }
    select(maxSocketNum + 1, &toMonitor, NULL, NULL, NULL);

    int signal;
    for (int i = 0; i < connected.size(); ++i) {
      if (FD_ISSET(connected[i], &toMonitor)) {
        signal = recv(connected[i], &potato, sizeof(potato), MSG_WAITALL);
        break;
      }
    }

    if (potato.remainHops == 0 || signal == 0) {
      break;
    }
    --potato.remainHops;
    // potato._trace.push_back(playerId);
    potato.trace[potato.nextToFill] = playerId;
    ++potato.nextToFill;
    if (potato.remainHops == 0) {
      send(master, &potato, sizeof(potato), 0);
      cout << "I'm it" << endl;
    } else {
      int pick = rand() % 2;
      int toSendSocket = connected[pick];
      int toSendId = neighborIds[pick];
      send(toSendSocket, &potato, sizeof(potato), 0);
      cout << "Sending potato to " << toSendId << endl;
    }
  }

  for (int i = 0; i < connected.size(); ++i) {
    close(connected[i]);
  }
  return EXIT_SUCCESS;
}