#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cstring>
#include <iostream>
#include <string>
using namespace std;

int makeServer(const char* port);
int connectToServer(const char* hostName, const char* port);
int acceptClient(int socket, string& ip);
int getPortNumber(int socket);