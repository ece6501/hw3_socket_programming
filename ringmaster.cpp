#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <algorithm>
#include <vector>

#include "potato.hpp"
#include "socketutils.hpp"

using namespace std;

int main(int argc, char* argv[]) {
  if (argc != 4) {
    cerr << "Usage: ./ringmaster <port_num> <num_players> <num_hops>" << endl;
    return EXIT_FAILURE;
  }

  const char* port = argv[1];
  int numPlayers = atoi(argv[2]), numHops = atoi(argv[3]);

  if (numPlayers <= 1) {
    cerr << "Player number should be greater than 1." << endl;
    return EXIT_FAILURE;
  }

  if (numHops < 0 || numHops > 512) {
    cerr << "Number of hops should in range[0, 512]" << endl;
    return EXIT_FAILURE;
  }

  vector<int> playersSocket;
  vector<int> playersPort;
  vector<string> playersIp;

  cout << "Potato Ringmaster" << endl;
  cout << "Players = " << numPlayers << endl;
  cout << "Hops = " << numHops << endl;

  int listen_fd = makeServer(port);

  /* Start to accepting players. */
  for (int i = 0; i < numPlayers; ++i) {
    int playerPort;
    string playerIpReader;
    int connect_fd = acceptClient(listen_fd, playerIpReader);
    send(connect_fd, &i, sizeof(i), 0);  // tell player its number
    send(connect_fd, &numPlayers, sizeof(numPlayers),
         0);  // tell player the total player number
    recv(connect_fd, &playerPort, sizeof(playerPort), MSG_WAITALL);
    playersSocket.push_back(connect_fd);
    playersPort.push_back(playerPort);
    playersIp.push_back(playerIpReader);
    cout << "Player " << i << " is ready to play" << endl;
  }

  /* Build the circle. */
  for (int i = 0; i < numPlayers; ++i) {
    int right = (i + 1) % numPlayers;
    int rightPort = playersPort[right];
    string& ip = playersIp[right];
    int ipCharLen = ip.size() + 1;
    char rightIp[ipCharLen];
    memcpy(rightIp, ip.c_str(), ipCharLen);
    send(playersSocket[i], &rightPort, sizeof(rightPort), 0);
    send(playersSocket[i], &ipCharLen, sizeof(ipCharLen), 0);
    send(playersSocket[i], rightIp, sizeof(rightIp), 0);
  }

  /* Game starts. */
  Potato potato;
  potato.remainHops = numHops;
  if (numHops == 0) {
    for (int i = 0; i < numPlayers; ++i) {
      send(playersSocket[i], &potato, sizeof(potato), 0);
    }
    return EXIT_SUCCESS;
  }

  srand((unsigned int)time(NULL) + numPlayers);
  int playerId = rand() % numPlayers;
  send(playersSocket[playerId], &potato, sizeof(potato), 0);
  cout << "Ready to start the game, sending potato to player " << playerId
       << endl;

  // Monitor the potato to be sent back
  fd_set players;
  FD_ZERO(&players);
  for (int i = 0; i < numPlayers; ++i) {
    FD_SET(playersSocket[i], &players);
  }
  int maxSocketNum = *(max_element(playersSocket.begin(), playersSocket.end()));
  select(maxSocketNum + 1, &players, NULL, NULL, NULL);
  for (int i = 0; i < numPlayers; ++i) {
    if (FD_ISSET(playersSocket[i], &players)) {
      recv(playersSocket[i], &potato, sizeof(potato), MSG_WAITALL);
      break;
    }
  }

  /* Close game. */
  for (int i = 0; i < numPlayers; ++i) {
    send(playersSocket[i], &potato, sizeof(potato), 0);
  }

  cout << "Trace of potato:" << endl;
  for (int i = 0; i < potato.nextToFill - 1; ++i) {
    cout << potato.trace[i] << ",";
  }
  cout << potato.trace[potato.nextToFill - 1] << endl;

  for (int i = 0; i < numPlayers; ++i) {
    close(playersSocket[i]);
  }
  close(listen_fd);
  return EXIT_SUCCESS;
}