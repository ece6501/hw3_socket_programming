#include <iostream>
#include <cstring>

using namespace std;
// POD design
class Potato {
 public:
  int remainHops;
  int nextToFill;
  int trace[512];

  Potato() : remainHops(0), nextToFill(0) { memset(trace, 0, sizeof(trace)); }  
};